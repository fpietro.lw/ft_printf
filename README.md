# ft_printf

## Description

This project is part of the [42 cursus][1], and aims to replicate the `printf` function.

Currently, the following conversion specifiers are supported:
Specifier | Description
:---: | :---
`%d`/`%i` | Prints the <ins>int</ins> argument in signed decimal notation.
`%x`/`%X` | Prints the <ins>unsigned int</ins> argument in unsigned hexadecimal notation. The output's case matches the specifier's.
`%u` | Prints the <ins>unsigned int</ins> argument in unsigned decimal notation.
`%c` | Prints the <ins>char</ins> argument.
`%s` | Prints the <ins>string</ins> argument.
`%%` | Prints the **%** character.


## Notes

The current version of this project complies to the specifications of the Mandatory Part only, and therefore no flags have been implemented yet.
The aforementioned feature will be added later this year, and this project will afterwards be merged with [libft][2].



[1]: https://42lisboa.com/
[2]: https://gitlab.com/fpietro.lw/libft
