# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fmouronh <fmouronh@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/11/27 00:13:24 by fmouronh          #+#    #+#              #
#    Updated: 2022/11/27 00:13:32 by fmouronh         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRCS = src/ft_format.c \
		src/ft_get_char.c \
		src/ft_get_hex.c \
		src/ft_get_int.c \
		src/ft_get_percent.c \
		src/ft_get_ptr.c \
		src/ft_get_str.c \
		src/ft_get_uns_int.c \
		src/ft_parse_arg.c \
		src/ft_print_arg.c \
		src/ft_printf.c

OBJS = ft_format.o \
			ft_get_char.o \
			ft_get_hex.o \
			ft_get_int.o \
			ft_get_percent.o \
			ft_get_ptr.o \
			ft_get_str.o \
			ft_get_uns_int.o \
			ft_parse_arg.o \
			ft_print_arg.o \
			ft_printf.o

LIBFT = libft/libft.a
PRINTF = ft_printf.a
NAME = libftprintf.a

CC = cc
CFLAGS = -Wall -Wextra -Werror -I./includes/ -ggdb -c

all: $(NAME)

$(NAME): $(LIBFT) $(PRINTF)
	ar rcsT $(NAME) $(PRINTF) $(LIBFT)

$(LIBFT):
	cd libft/
	make -C libft/

$(PRINTF): $(OBJS)
	ar rcs $(PRINTF) $(OBJS)

$(OBJS): $(SRCS)
	$(CC) $(CFLAGS) $(SRCS)

clean:
	rm -f $(OBJS)
	make -C libft/ clean

fclean: clean
	rm -f $(NAME) $(PRINTF) $(LIBFT)

re: fclean all
