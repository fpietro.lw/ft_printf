/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_int.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmouronh <fmouronh@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/22 00:52:04 by fmouronh          #+#    #+#             */
/*   Updated: 2022/11/27 00:14:49 by fmouronh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

char	*ft_get_int(int arg)
{
	char	*result;

	result = ft_itoa(arg);
	if (!result)
		return (NULL);
	return (result);
}
