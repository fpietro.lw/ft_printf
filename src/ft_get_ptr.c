/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_ptr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmouronh <fmouronh@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/22 21:46:52 by fmouronh          #+#    #+#             */
/*   Updated: 2022/11/27 00:14:50 by fmouronh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

char	*ft_get_ptr(unsigned long arg)
{
	char	*result;
	char	*hex;

	if (arg)
	{
		hex = ft_btox(&arg, sizeof(long int));
		result = ft_calloc(ft_strlen(hex) + 3, sizeof(char));
		if (!result)
			return (NULL);
		ft_strlcpy(result, "0x", 3);
		ft_strlcat(result, hex, ft_strlen(hex) + 3);
		free(hex);
	}
	else
		result = ft_strdup("(nil)");
	return (result);
}

/*
	extra leading + trailing zeroes must be handled
	also, this thing is doing the conversion backwards!
*/
