/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmouronh <fmouronh@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/21 20:33:30 by fmouronh          #+#    #+#             */
/*   Updated: 2023/03/21 20:33:30 by fmouronh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_printf(const char *str, ...)
{
	va_list	args;
	int		printed;
	int		i;

	va_start(args, str);
	printed = 0;
	i = 0;
	while (str[i])
	{
		if (str[i] == '%')
			printed += ft_parse_arg(&args, &str[++i]);
		else
		{
			ft_putchar_fd(str[i], 1);
			printed++;
		}
		i++;
	}
	return (printed);
}
