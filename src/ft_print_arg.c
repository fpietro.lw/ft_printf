/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_arg.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmouronh <fmouronh@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/22 21:46:52 by fmouronh          #+#    #+#             */
/*   Updated: 2022/11/27 00:14:50 by fmouronh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_print_arg(char *arg, char cspec)
{
	int	len;

	if (!arg)
	{
		if (cspec == 's')
			arg = ft_strdup("(null)");
		else if (cspec == 'p')
			arg = ft_strdup("(nil)");
	}
	if (!*arg && cspec == 'c')
	{
		write(1, "\0", 1);
		len = 1;
	}
	else
	{
		len = ft_strlen(arg);
		ft_putstr_fd(arg, 1);
	}
	if (arg)
		free(arg);
	return (len);
}
