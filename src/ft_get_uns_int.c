/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_uns_int.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmouronh <fmouronh@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/22 21:46:52 by fmouronh          #+#    #+#             */
/*   Updated: 2022/11/27 00:14:50 by fmouronh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

char	*ft_get_uns_int(unsigned int arg)
{
	char	*result;

	result = ft_uitoa(arg);
	if (!result)
		return (NULL);
	return (result);
}
