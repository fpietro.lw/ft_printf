/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_hex.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmouronh <fmouronh@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/22 00:52:04 by fmouronh          #+#    #+#             */
/*   Updated: 2022/11/27 00:14:49 by fmouronh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

char	*ft_get_hex(unsigned int arg, char cspec)
{
	char	*result;
	size_t	i;

	result = ft_btox(&arg, sizeof(int));
	if (!result)
		return (NULL);
	i = 0;
	if (cspec == 'X')
	{
		while (result[i])
		{
			result[i] = (char)ft_toupper((int)result[i]);
			i++;
		}
	}
	return (result);
}
