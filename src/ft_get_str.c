/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_str.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmouronh <fmouronh@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/22 00:52:04 by fmouronh          #+#    #+#             */
/*   Updated: 2022/11/27 00:14:50 by fmouronh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

char	*ft_get_str(char *arg)
{
	char	*result;

	if (arg)
		result = ft_strdup(arg);
	else
		result = NULL;
	return (result);
}
