/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_format.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmouronh <fmouronh@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/22 00:52:04 by fmouronh          #+#    #+#             */
/*   Updated: 2022/11/27 00:14:49 by fmouronh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_format(va_list *args, char cspec)
{
	char	*arg_fmt;
	int		arg_len;

	arg_fmt = NULL;
	arg_len = 0;
	if (cspec == 'c')
		arg_fmt = ft_get_char(va_arg(*args, int));
	else if (cspec == 's')
		arg_fmt = ft_get_str(va_arg(*args, char *));
	else if (cspec == 'p')
		arg_fmt = ft_get_ptr(va_arg(*args, unsigned long int));
	else if (cspec == 'd' || cspec == 'i')
		arg_fmt = ft_get_int(va_arg(*args, int));
	else if (cspec == 'u')
		arg_fmt = ft_get_uns_int(va_arg(*args, unsigned int));
	else if (cspec == 'x' || cspec == 'X')
		arg_fmt = ft_get_hex(va_arg(*args, unsigned int), cspec);
	else if (cspec == '%')
		arg_fmt = ft_get_percent();
	arg_len = ft_print_arg(arg_fmt, cspec);
	return (arg_len);
}
