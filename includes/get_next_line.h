#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <linux/limits.h>
# include "libft.h"

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 42
# endif

char	*get_next_line(int fd);
int		ft_invalid_fd(int fd, char *buffer);
int		ft_read_buffer(char *buffer);
char	*ft_cp_to_line(char *line, char *buffer, int eol);
void	ft_update_buffer(char *buffer, int eol);

#endif
