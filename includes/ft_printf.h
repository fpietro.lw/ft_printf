/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmouronh <fmouronh@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/22 00:52:30 by fmouronh          #+#    #+#             */
/*   Updated: 2022/11/27 00:14:07 by fmouronh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include "libft.h"

int		ft_printf(const char *str, ...);
int		ft_parse_arg(va_list *args, const char *str);
int		ft_format(va_list *args, char fmt_spec);
char	*ft_get_char(int arg);
char	*ft_get_str(char *arg);
char	*ft_get_ptr(unsigned long int arg);
char	*ft_get_int(int arg);
char	*ft_get_uns_int(unsigned int arg);
char	*ft_get_hex(unsigned int arg, char cspec);
char	*ft_get_percent(void);
char	*ft_get_ptr(unsigned long arg);
int		ft_print_arg(char *arg, char cspec);

#endif